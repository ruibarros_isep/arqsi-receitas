var config = {};

config.web = {};
config.mongodb = {};
config.medicamentosApi = {};
config.smtp = {};
config.smtp.mailgun = {};

config.web.secret = "PMeLwRxnvVr6KD3evNiAI4FQunvrFkLt7PSbhH3rdwoXE3tB";
config.web.tokens_ttl = 86400;
config.web.port = process.env.PORT;
config.web.domain = 'http://api-angular.azurewebsites.net/';

config.mongodb.host = 'sa:123@ds042677.mlab.com';
config.mongodb.port = '42677';
config.mongodb.database = 'apiweb-mongo-db-receitas';

config.medicamentosApi.url = 'http://apiweb-gestaomedicamentos.azurewebsites.net/api/';
config.medicamentosApi.email = '1151050@isep.ipp.pt';
config.medicamentosApi.password = 'Isep1151050!';
config.medicamentosApi.medicamento = '/Medicamentos';
config.medicamentosApi.apresentacao = '/Apresentacoes';
config.medicamentosApi.posologia = '/Posologias';
config.medicamentosApi.farmaco = '/Farmacos';
config.medicamentosApi.comentario = '/Comentarios';
config.medicamentosApi.token = '/Account/Token';

config.smtp.mailgun.username = 'api';
config.smtp.mailgun.apiKey = 'key-ab6760b64335517cd502251ea73b3d4a';
config.smtp.mailgun.sender = 'Gestão de Receitas <postmaster@sandbox1fa7763cd5774dda8b0241ed38e5b331.mailgun.org>';
config.smtp.mailgun.domain = 'sandbox1fa7763cd5774dda8b0241ed38e5b331.mailgun.org';


module.exports = config;
