FROM node:8.9

WORKDIR /src/app

COPY package.json .

RUN npm install pm2 -g
RUN npm install

COPY . .

EXPOSE 3000 5858
CMD [ "npm", "run-script", "dev" ]
