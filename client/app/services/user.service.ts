import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';

const URL = 'api/user';

@Injectable()
export class UserService {


  constructor(private httpClient: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(URL, this.getStandardHeaders());
  }

  public getUser(id: string): Observable<User> {
    return this.httpClient.get<User>(URL + '/' + id, this.getStandardHeaders());
  }

  public createUser(user: User): Observable<User> {
    return this.httpClient.post<User>(URL, user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  private getStandardHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.authenticationService.getToken()
      })
    };
  }

}
