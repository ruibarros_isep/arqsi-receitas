import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs/Observable';
import {Receita} from '../models/receita';
import {Prescricao} from '../models/prescricao';

const URL = 'api/receita';

@Injectable()
export class ReceitasService {

  constructor(private httpClient: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  getReceitasByUtente(): Observable<Receita[]> {
    return this.httpClient.get<Receita[]>(URL, this.getStandardHeaders());
  }

  getReceitasByMedico(): Observable<Receita[]> {
    return this.httpClient.get<Receita[]>(URL + '/medico', this.getStandardHeaders());
  }

  getReceita(id: string): Observable<Receita> {
    return this.httpClient.get<Receita>(URL + '/' + id, this.getStandardHeaders());
  }

  getPrescicaoById(receitaId: string, id: string): Observable<Prescricao> {
    return this.httpClient.get<Prescricao>(URL + '/' + receitaId + '/' + 'prescricao/' + id, this.getStandardHeaders());
  }

  updateAlertaPrescricao(receitaId: string, prescricaoId: string, alerta: Date): Observable<Prescricao> {
    return this.httpClient.put<Prescricao>(
      URL + '/' + receitaId + '/' + 'prescricao/' + prescricaoId + '/alerta', {alerta: alerta}, this.getStandardHeaders()
    );
  }

  createReceita(utenteId: string): Observable<Receita> {
    return this.httpClient.post<Receita>(URL, {utente: utenteId}, this.getStandardHeaders());
  }

  createPrescricao(receitaId: string, apresentcaoId: string, posologiaId: string, quantidade: number): Observable<Receita> {

    const body = {
      apresentacao: apresentcaoId,
      posologia: posologiaId,
      quantidade: quantidade
    };
    return this.httpClient.post<Receita>(URL + '/' + receitaId + '/prescricao', body, this.getStandardHeaders());
  }

  updatePrescricao(receitaId: string, prescricaoId: string, apresentcaoId: string, posologiaId: string, quantidade: number): Observable<Prescricao> {

    const body = {
      apresentacao: apresentcaoId,
      posologia: posologiaId,
      quantidade: quantidade
    };
    return this.httpClient.put<Prescricao>(URL + '/' + receitaId + '/prescricao/' + prescricaoId, body, this.getStandardHeaders());
  }

  aviarPrescricao(receitaId: string, prescricaoId: string, quantidade: number): Observable<Prescricao> {
    return this.httpClient.put<Prescricao>(
      URL + '/' + receitaId + '/prescricao/' + prescricaoId + '/aviar',
      {quantidade: quantidade},
      this.getStandardHeaders()
    );
  }

  private getStandardHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.authenticationService.getToken()
      })
    };
  }

}
