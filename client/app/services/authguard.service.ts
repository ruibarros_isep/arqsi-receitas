import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {User} from '../models/user';
import {Receita} from '../models/receita';

@Injectable()
export class AuthGuardService implements CanActivate {

  user: User;

  constructor(private authenticationService: AuthenticationService,
              private router: Router) {
    this.user = this.authenticationService.getAuthUser();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    this.user = this.authenticationService.getAuthUser();
    if (!this.user) {
      this.authenticationService.redirectUrl = state.url;
      this.router.navigate(['/login']);
      return false;
    }

    if (state.url === '/receitas') {
      if (this.userIsFarmaceutico()) {
        this.router.navigate(['/receitas/procurar']);
        return false;
      }
    }

    if (state.url === '/receitas/procurar') {
      if (!this.userIsFarmaceutico()) {
        this.router.navigate(['/unauthorized']);
        return false;
      }
    }

    return true;
  }

  userIsMedico(): boolean {
    this.user = this.authenticationService.getAuthUser();

    if (this.user) {
      return this.user.medico;
    }
    return false;
  }

  userIsFarmaceutico(): boolean {
    this.user = this.authenticationService.getAuthUser();

    if (this.user) {
      return this.user.farmaceutico;
    }
    return false;
  }

  userIsUtente(): boolean {
    this.user = this.authenticationService.getAuthUser();

    if (this.user) {
      return !this.user.farmaceutico && !this.user.medico;
    }
    return false;
  }

  getUser(): User {
    return this.authenticationService.getAuthUser();
  }

  isMedicoOfReceita(receita: Receita): boolean {
    this.user = this.authenticationService.getAuthUser();

    if (this.user === null) {
      return false;
    }

    return receita.medico._id === this.user._id;
  }
}
