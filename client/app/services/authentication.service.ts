import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Credentials} from '../models/credentials';
import 'rxjs/add/operator/map';
import {LoginResponse} from '../models/loginResponse';
import {Router} from '@angular/router';
import {isUndefined} from 'util';
import {User} from '../models/user';
import * as jwt_decode from 'jwt-decode';

const LOGIN_URL = 'api/auth/login';
const DEFAULT_REDIRECT = '/';
const TOKEN_KEY = 'id_token';

@Injectable()
export class AuthenticationService {

  redirectUrl: string;
  authUser: User;

  constructor(private httpClient: HttpClient,
              private router: Router) {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token !== null) {
      this.decodeTokenAndSetAuthUser(token);
    }
  }

  login(credentials: Credentials): any {
    this.httpClient.post<LoginResponse>(LOGIN_URL, credentials)
      .subscribe(
        data => {
          localStorage.setItem(TOKEN_KEY, data.token);
          this.decodeTokenAndSetAuthUser(data.token);

          const redirect = isUndefined(this.redirectUrl) ? DEFAULT_REDIRECT : this.redirectUrl;
          this.router.navigate([redirect]);
          return true;
        },
        error => {
          return error;
        }
      );
  }

  loggedIn(): boolean {
    const token = this.getToken();
    return token !== null;
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  logout() {
    localStorage.removeItem(TOKEN_KEY);
    this.authUser = null;
    this.router.navigate(['/login']);
  }

  getAuthUser(): User {
    return this.authUser;
  }

  private decodeTokenAndSetAuthUser(token: string): void {
    const tokenDecoded = jwt_decode(token);
    this.authUser = tokenDecoded.user;
  }
}
