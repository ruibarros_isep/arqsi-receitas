import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Medicamento} from '../models/medicamento';
import {Apresentacao} from '../models/apresentacao';
import {Comentario} from '../models/comentario';
import {AuthenticationService} from './authentication.service';

const URL = 'api/medicamentos';


@Injectable()
export class MedicamentosService {

  constructor(private httpClient: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  getMedicamentos(): Observable<Medicamento[]> {
    return this.httpClient.get<Medicamento[]>(URL);
  }

  getApresentacoesByMedicamentoId(id: string): Observable<Apresentacao[]> {
    return this.httpClient.get<Apresentacao[]>(URL + '/' + id + '/apresentacoes');
  }

  postComentario(apresentacaoId: string, comentario: string): Observable<Comentario> {
    return this.httpClient.post<Comentario>(
      URL + '/apresentacao/' + apresentacaoId + '/comentario',
      {comentario: comentario},
      this.getStandardHeaders()
    );
  }

  private getStandardHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.authenticationService.getToken()
      })
    };
  }

}
