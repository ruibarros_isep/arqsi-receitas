import { User } from './models/user';

export const USERS: User[] = [
  { _id: '11', nome: 'Mr. Nice', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '12', nome: 'Narco', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '13', nome: 'Bombasto', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '14', nome: 'Celeritas', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '15', nome: 'Magneta', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '16', nome: 'RubberMan', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '17', nome: 'Dynama', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '18', nome: 'Dr IQ', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '19', nome: 'Magma', email: 'test@test.com', medico: true, farmaceutico: true },
  { _id: '20', nome: 'Tornado', email: 'test@test.com', medico: true, farmaceutico: true }
];
