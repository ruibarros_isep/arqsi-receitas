import {Aviamento} from './aviamento';

export class Prescricao {
  _id: string;
  apresentacao: string;
  apresentacaoId: string;
  posologia: string;
  posologiaId: string;
  farmaco: string;
  quantidade: number;
  aviada: Date;
  alerta: Date;
  validade: Date;
  aviamentos: Aviamento[];
}
