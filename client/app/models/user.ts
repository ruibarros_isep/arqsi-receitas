export class User {
  _id: string;
  nome: string;
  email: string;
  medico ?: boolean;
  farmaceutico ?: boolean;
  password ?: string;
}
