import {Prescricao} from './prescricao';
import {User} from './user';

export class Receita {
  _id: string;
  nome: string;
  utente: User;
  medico: User;
  data: Date;
  prescricoes: Prescricao[];
}
