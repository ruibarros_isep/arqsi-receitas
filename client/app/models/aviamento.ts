import {User} from './user';

export class Aviamento {
  _id: string;
  farmaceutico: User;
  data: Date;
  quantidade: number;
}
