import {Posologia} from './posologia';
import {Comentario} from './comentario';

export class Apresentacao {
  id: string;
  forma: string;
  concentracao: string;
  qtd: string;
  posologias: Posologia[];
  comentarios: Comentario[];
}
