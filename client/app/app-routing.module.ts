import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './components/users/users.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuardService} from './services/authguard.service';
import {PageNotFoundComponent} from './components/pagenotfound/pagenotfound.component';
import {RegisterComponent} from './components/register/register.component';
import {ReceitasComponent} from './components/receitas/receitas.component';
import {ReceitaDetailComponent} from './components/receita-detail/receita-detail.component';
import {PrescricaoComponent} from './components/prescricao/prescricao.component';
import {UnauthorizedComponent} from './components/unauthorized/unauthorized.component';
import {ReceitaSearchComponent} from './components/receita-search/receita-search.component';
import {MedicamentosComponent} from './components/medicamentos/medicamentos.component';
import {MedicamentoDetailComponent} from './components/medicamento-detail/medicamento-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/receitas',
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'unauthorized',
    component: UnauthorizedComponent
  }, {
    path: 'register',
    component: RegisterComponent
  }, {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuardService]
  }, {
    path: 'users/:id',
    component: UserDetailComponent,
    canActivate: [AuthGuardService]
  }, {
    path: 'medicamentos',
    component: MedicamentosComponent,
  }, {
    path: 'medicamentos/:id',
    component: MedicamentoDetailComponent,
  }, {
    path: 'receitas',
    component: ReceitasComponent,
    canActivate: [AuthGuardService]
  }, {
    path: 'receita/:id',
    component: ReceitaDetailComponent,
    canActivate: [AuthGuardService]
  }, {
    path: 'receitas/procurar',
    component: ReceitaSearchComponent,
    canActivate: [AuthGuardService]
  }, {
    path: 'receita/:id/prescricao/:prescricao_id',
    component: PrescricaoComponent,
    canActivate: [AuthGuardService]
  }, {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {
}
