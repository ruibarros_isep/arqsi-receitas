import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './/app-routing.module';
import {AppComponent} from './app.component';
import {UsersComponent} from './components/users/users.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {PageNotFoundComponent} from './components/pagenotfound/pagenotfound.component';
import {LoginComponent} from './components/login/login.component';
import {UserService} from './services/user.service';
import {AuthenticationService} from './services/authentication.service';
import {AuthGuardService} from './services/authguard.service';
import {RegisterComponent} from './components/register/register.component';
import {ReceitasComponent} from './components/receitas/receitas.component';
import {ReceitasService} from './services/receitas.service';
import {ReceitaDetailComponent} from './components/receita-detail/receita-detail.component';
import {PrescricaoComponent} from './components/prescricao/prescricao.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NavigationComponent} from './components/navigation/navigation.component';
import {ReceitaFormComponent} from './components/receita-form/receita-form.component';
import {UnauthorizedComponent} from './components/unauthorized/unauthorized.component';
import {PrescricaoFormComponent} from './components/prescricao-form/prescricao-form.component';
import {MedicamentosService} from './services/medicamentos.service';
import {ReceitaSearchComponent} from './components/receita-search/receita-search.component';
import {MedicamentosComponent} from './components/medicamentos/medicamentos.component';
import {MedicamentoDetailComponent} from './components/medicamento-detail/medicamento-detail.component';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressHttpClientModule} from '@ngx-progressbar/http-client';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserDetailComponent,
    LoginComponent,
    PageNotFoundComponent,
    RegisterComponent,
    ReceitasComponent,
    ReceitaDetailComponent,
    PrescricaoComponent,
    NavigationComponent,
    ReceitaFormComponent,
    UnauthorizedComponent,
    PrescricaoFormComponent,
    ReceitaSearchComponent,
    MedicamentosComponent,
    MedicamentoDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    NgProgressModule.forRoot(),
    NgProgressHttpClientModule,

  ],
  providers: [UserService, AuthenticationService, AuthGuardService, ReceitasService, MedicamentosService],
  bootstrap: [AppComponent],
  entryComponents: [
    PrescricaoFormComponent
  ]
})
export class AppModule {
}
