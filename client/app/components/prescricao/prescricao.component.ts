import {Component, OnInit} from '@angular/core';
import {ReceitasService} from '../../services/receitas.service';
import {Prescricao} from '../../models/prescricao';
import {ActivatedRoute} from '@angular/router';
import {AuthGuardService} from '../../services/authguard.service';
import {PrescricaoFormComponent} from '../prescricao-form/prescricao-form.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-prescricao',
  templateUrl: './prescricao.component.html',
  styleUrls: ['./prescricao.component.css']
})
export class PrescricaoComponent implements OnInit {

  prescricao: Prescricao;
  today = new Date();
  prazoTerminado: boolean;
  receitaId: string;
  prescricaoId: string;
  alerta;
  quantidade: number;
  aviamentoError: string;

  constructor(private receitasService: ReceitasService,
              private route: ActivatedRoute,
              private authorizationService: AuthGuardService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.receitaId = this.route.snapshot.paramMap.get('id');
    this.prescricaoId = this.route.snapshot.paramMap.get('prescricao_id');
    this.getPrescricao();
  }


  getPrescricao() {
    this.receitasService.getPrescicaoById(this.receitaId, this.prescricaoId)
      .subscribe(prescricao => {
        this.prescricao = prescricao;
        this.prazoTerminado = (this.today > new Date(prescricao.validade));
      });
  }

  guardarAlerta(): void {
    const date = new Date(this.alerta.year, this.alerta.month - 1, this.alerta.day);
    this.receitasService.updateAlertaPrescricao(this.receitaId, this.prescricaoId, date)
      .subscribe(prescricao => {
          this.prescricao = prescricao;
        },
        error => {
          console.log(error);
        });
  }

  userIsFarmaceutico(): boolean {
    return this.authorizationService.userIsFarmaceutico();
  }

  userIsUtente(): boolean {
    return this.authorizationService.userIsUtente();
  }

  userIsMedico(): boolean {
    return this.authorizationService.userIsMedico();
  }

  aviarPrescricao() {
    this.receitasService.aviarPrescricao(this.receitaId, this.prescricaoId, this.quantidade).subscribe(
      prescricao => {
        this.prescricao = prescricao;
        this.aviamentoError = null;
      }, error => {
        this.aviamentoError = error.error;
      }
    );
  }

  openPrescricaoForm() {
    const modalRef = this.modalService.open(PrescricaoFormComponent);
    modalRef.componentInstance.receitaId = this.receitaId;
    modalRef.componentInstance.prescricao = this.prescricao;

    modalRef.result.then(prescricao => {
      this.prescricao = prescricao;
    }).catch(reason => console.log(reason));
  }
}
