import {Component, Input, OnInit} from '@angular/core';
import {ReceitasService} from '../../services/receitas.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Medicamento} from '../../models/medicamento';
import {MedicamentosService} from '../../services/medicamentos.service';
import {Apresentacao} from '../../models/apresentacao';
import {Posologia} from '../../models/posologia';
import {Prescricao} from '../../models/prescricao';

@Component({
  selector: 'app-prescricao-form',
  templateUrl: './prescricao-form.component.html',
  styleUrls: ['./prescricao-form.component.css']
})
export class PrescricaoFormComponent implements OnInit {

  @Input() receitaId: string;
  @Input() prescricao: Prescricao;

  quantidade: number;
  medicamentos: Medicamento[];
  apresentacoes: Apresentacao[];
  selectedMedicamento: Medicamento;
  selectedApresentacao: Apresentacao;
  selectedPosologia: Posologia;
  error: string;

  constructor(private receitaService: ReceitasService,
              private medicamentosService: MedicamentosService,
              public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    this.medicamentosService.getMedicamentos().subscribe(medicamentos => {
      this.medicamentos = medicamentos;
    });
  }

  createPrescricao() {
    this.receitaService.createPrescricao(
      this.receitaId,
      this.selectedApresentacao.id,
      this.selectedPosologia.posologiaId,
      this.quantidade
    ).subscribe(
      receita => {
        this.activeModal.close(receita);
      }, error => {
        this.error = error.error;
      });
  }

  updatePrescricao() {
    this.receitaService.updatePrescricao(
      this.receitaId,
      this.prescricao._id,
      this.selectedApresentacao.id,
      this.selectedPosologia.posologiaId,
      this.quantidade
    ).subscribe(prescricao => {
      this.activeModal.close(prescricao);
    }, error => {
      this.error = error.error;
    });
  }

  loadApresentacoes() {
    this.medicamentosService.getApresentacoesByMedicamentoId(this.selectedMedicamento.id).subscribe(
      apresentacoes => {
        this.apresentacoes = apresentacoes;
      }
    );
  }
}
