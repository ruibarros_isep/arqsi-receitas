import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescricaoFormComponent } from './prescricao-form.component';

describe('PrescricaoFormComponent', () => {
  let component: PrescricaoFormComponent;
  let fixture: ComponentFixture<PrescricaoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescricaoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescricaoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
