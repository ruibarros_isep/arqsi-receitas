import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Credentials} from '../../models/credentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: Credentials;
  error: string;

  constructor(private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.credentials = new Credentials();
  }

  onLogin() {
    if (!this.authenticationService.login(this.credentials)) {
        this.error = 'Email ou password inválidas';
    }
  }

}
