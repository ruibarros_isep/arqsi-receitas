import {Component, OnInit} from '@angular/core';
import {ReceitasService} from '../../services/receitas.service';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-receita-form',
  templateUrl: './receita-form.component.html',
  styleUrls: ['./receita-form.component.css']
})
export class ReceitaFormComponent implements OnInit {

  users: User[];
  selectedUtente: User;

  constructor(private receitasService: ReceitasService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  createReceita(utente: string): void {
    this.receitasService.createReceita(utente).subscribe(receita => {
        this.router.navigate(['/receita/' + receita._id]);
    });
  }

}
