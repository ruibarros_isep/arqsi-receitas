import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  error: string;

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {

    // TODO this can be changed to allow the creation of medico and  farmaceutico
    this.user = new User();
    this.user.medico = false;
    this.user.farmaceutico = false;
  }

  createUser() {
    this.userService.createUser(this.user)
      .subscribe(user => {
          this.router.navigate(['']);
        },
        error => {
          this.error = 'Email já utilizado';
        });
  }

}
