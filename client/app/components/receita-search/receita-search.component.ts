import {Component, OnInit} from '@angular/core';
import {ReceitasService} from '../../services/receitas.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-receita-search',
  templateUrl: './receita-search.component.html',
  styleUrls: ['./receita-search.component.css']
})
export class ReceitaSearchComponent implements OnInit {

  receitaId: string;
  error: string;

  constructor(private receitaService: ReceitasService,
              private router: Router) {
  }

  ngOnInit() {
  }

  searchReceita() {
    this.receitaService.getReceita(this.receitaId).subscribe(receita => {
      this.router.navigate(['/receita/' + receita._id]);
    }, error => {
      this.error = 'Receita não encontrada';
    });
  }
}
