import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MedicamentosService} from '../../services/medicamentos.service';
import {Apresentacao} from '../../models/apresentacao';
import {AuthGuardService} from '../../services/authguard.service';

@Component({
  selector: 'app-medicamento-detail',
  templateUrl: './medicamento-detail.component.html',
  styleUrls: ['./medicamento-detail.component.css']
})
export class MedicamentoDetailComponent implements OnInit {

  apresentacoes: Apresentacao[];
  selectedApresentcao: Apresentacao;
  comentario = '';

  constructor(private route: ActivatedRoute,
              private medicamentosService: MedicamentosService,
              private authorizationService: AuthGuardService) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.medicamentosService.getApresentacoesByMedicamentoId(id).subscribe(apresentacoes => {
      this.apresentacoes = apresentacoes;
    });
  }

  updatePosologias(apresentacao: Apresentacao) {
    this.selectedApresentcao = apresentacao;
  }

  closePosologias() {
    this.selectedApresentcao = null;
  }

  userIsMedico(): boolean {
    return this.authorizationService.userIsMedico();
  }

  postComentario() {
    this.medicamentosService.postComentario(this.selectedApresentcao.id, this.comentario)
      .subscribe(comentario => {
          this.selectedApresentcao.comentarios.push(comentario);
          this.comentario = '';
      });
  }
}
