import {Component, OnInit} from '@angular/core';
import {Receita} from '../../models/receita';
import {ReceitasService} from '../../services/receitas.service';
import {ActivatedRoute} from '@angular/router';
import {AuthGuardService} from '../../services/authguard.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PrescricaoFormComponent} from '../prescricao-form/prescricao-form.component';

@Component({
  selector: 'app-receita-detail',
  templateUrl: './receita-detail.component.html',
  styleUrls: ['./receita-detail.component.css']
})
export class ReceitaDetailComponent implements OnInit {

  receita: Receita;
  today = new Date().toISOString();

  constructor(private route: ActivatedRoute,
              private receitaService: ReceitasService,
              private authorizationService: AuthGuardService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.getReceita();
  }

  getReceita(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.receitaService.getReceita(id).subscribe(receita => this.receita = receita);
  }

  canCreatePrescricao(): boolean {
    return this.authorizationService.userIsMedico() && this.authorizationService.isMedicoOfReceita(this.receita);
  }

  openPrescricaoForm() {
    const modalRef = this.modalService.open(PrescricaoFormComponent);
    modalRef.componentInstance.receitaId = this.receita._id;

    modalRef.result.then(receita => {
      this.receita = receita;
    }).catch(reason => console.log(reason));
  }
}
