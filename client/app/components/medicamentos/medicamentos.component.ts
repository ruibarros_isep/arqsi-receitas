import {Component, OnInit} from '@angular/core';
import {MedicamentosService} from '../../services/medicamentos.service';
import {Medicamento} from '../../models/medicamento';

@Component({
  selector: 'app-medicamentos',
  templateUrl: './medicamentos.component.html',
  styleUrls: ['./medicamentos.component.css']
})
export class MedicamentosComponent implements OnInit {

  medicamentos: Medicamento[];

  constructor(private medicamentosService: MedicamentosService) {
  }

  ngOnInit() {
    this.medicamentosService.getMedicamentos().subscribe(
      medicamentos => {
        this.medicamentos = medicamentos;
      }
    );
  }

}
