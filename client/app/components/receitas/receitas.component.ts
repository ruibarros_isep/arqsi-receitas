import {Component, OnInit} from '@angular/core';
import {ReceitasService} from '../../services/receitas.service';
import {Receita} from '../../models/receita';
import {AuthGuardService} from '../../services/authguard.service';

@Component({
  selector: 'app-receitas',
  templateUrl: './receitas.component.html',
  styleUrls: ['./receitas.component.css']
})
export class ReceitasComponent implements OnInit {

  receitas: Receita[];

  constructor(private receitasService: ReceitasService,
              private authorizationService: AuthGuardService) {
  }

  ngOnInit() {
    this.getReceitas();
  }

  getReceitas(): void {

    if (this.authorizationService.userIsMedico()) {
      this.receitasService.getReceitasByMedico()
        .subscribe(receitas => {
          this.receitas = receitas;
        });
      return;
    }

    this.receitasService.getReceitasByUtente()
      .subscribe(receitas => {
        this.receitas = receitas;
      });
  }

  userIsMedico(): boolean {
    return this.authorizationService.userIsMedico();
  }

}
