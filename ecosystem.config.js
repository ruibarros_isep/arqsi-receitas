
module.exports = {
    apps : [
        {
            name: "receitas",
            script: "./server.js",
            env: { //production
                watch: true,
                "PORT": 80,
                "NODE_ENV": "production",
            },
            env_development: {
                watch: false,
                ignore_watch : ["node_modules"],
                "PORT": 3000,
                "NODE_ENV": "development",
            }
        }
    ]
};