
var config;

switch (process.env.NODE_ENV){
    case 'development':
        config = require("./config_dev");
        break;
    default:
        config = require("./config_prod");
}

module.exports = config;