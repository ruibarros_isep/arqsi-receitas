#Receitas API

## Development

#### Requirements

 - [docker](https://www.docker.com/)
 - [docker-compose](https://docs.docker.com/compose/install/)
 
 ```bash
    $ docker-compose -f docker-compose.yml up -d
 ```
 
 The app is running on: [http://127.0.0.1:3030/api/](http://127.0.0.1:3030/api/)
 
## Production

 The app is running on: [http://apiwebgestaoreceitas-1151050-1151071.azurewebsites.net/api/](http://apiwebgestaoreceitas-1151050-1151071.azurewebsites.net/api/)


## Usage

* Create an user (no auth)
```http
POST /api/user HTTP/1.1
Content-Type: application/json

{
  "nome": "Ze Farmaceutico",
  "email": "ze@hotmail.pt",
  "password": "farmaceutico",
  "medico": false,
  "farmaceutico": true
}
```

* Login the user (no auth)
```http
POST /api/auth/login HTTP/1.1
Content-Type: application/json

{
  "email": "ze@hotmail.pt",
  "password": "farmaceutico"
}

```

* Create Receita (medico)
```http
POST /api/receita HTTP/1.1
Content-Type: application/json
x-access-token: <token-generated>

{
  "nome": "Test",
  "utente": "<utente-id>",
}
```

* Create Prescricao (medico)
```http
POST /api/receita/<receita-id>/prescricao HTTP/1.1
x-access-token: <token-generated>
Content-Type: application/json

{
  "apresentacao": <apresentcao-id>,
  "posologia": <posologia-id>,
  "quantidade": 3
}
```

* Update Prescricao (medico)
```http
PUT /api/receita/<receita-id>/prescricao/<prescricao-id> HTTP/1.1
x-access-token: <token-generated>
Content-Type: application/json

{
	"apresentacao": 0,
	"posologia": 0,
	"quantidade": 10
}
```

* Create Aviamento (farmaceutico)
```http
PUT /api/receita/<reeceita-id>/prescricao/<prescricao-id>/aviar HTTP/1.1
x-access-token: <token-generated>
Content-Type: application/json

{
  "quantidade": 1
}
```

* Get Receita (user owner / medico / farmaceutico)
```http
GET /api/receita HTTP/1.1
x-access-token: <token-generated>

```

* Get Prescricao (user owner / medico / farmaceutico)
```http
GET /api/receita/<receita-id>/prescricao/<prescricao-id> HTTP/1.1
x-access-token: <token-generated>

```

* Prescricoes por aviar (user)
```http
GET /api/utente/<user-id>/prescricao/poraviar HTTP/1.1
x-access-token: <token-generated>

```