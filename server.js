var config = require("./config");
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");

var userRoutes = require("./server/route/userRoutes");
var receitaRoutes = require("./server/route/receitaRoutes");
var utenteRoutes = require("./server/route/utenteRoutes");
var authRoutes = require("./server/route/authRoutes");
var medicamentosRoutes = require("./server/route/medicamentosRoutes");

mongoose.connect(
  "mongodb://" +
    config.mongodb.host +
    ":" +
    config.mongodb.port +
    "/" +
    config.mongodb.database
);

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/receita", receitaRoutes);
app.use("/api/utente", utenteRoutes);
app.use("/api/medicamentos", medicamentosRoutes);

app.use(express.static(__dirname +  '/public'));
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

app.listen(config.web.port);
