#!/bin/bash

# we have to do this because azure does not allow upload greater than 52mb
cd ..

sudo rm -r arqsi-receitas-task-deploy

git clone https://ruibarros_isep@bitbucket.org/ruibarros_isep/arqsi-receitas.git arqsi-receitas-task-deploy

cd arqsi-receitas-task-deploy

sudo npm install mongoose
sudo npm install mailgun.js
sudo npm install mongoose-id-validator
sudo npm install bcryptjs

cd ..

zip -r receitas.zip arqsi-receitas-task-deploy

sudo rm -r arqsi-receitas-task-deploy

cd arqsi-receitas

