
function UserDTO(user) {
    this._id = user._id;
    this.nome = user.nome;
    this.email = user.email;
    this.medico = user.medico;
    this.farmaceutico = user.farmaceutico;
}

module.exports = UserDTO;