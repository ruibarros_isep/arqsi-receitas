var userService = require('../services/userService');
var UserDTO = require('../dto/userDTO');

var service = {};

function AviamentoDTO(aviamento, farmaceutico) {
  this._id = aviamento._id;
  this.data = aviamento.data;
  this.quantidade = aviamento.quantidade;
  this.farmaceutico = farmaceutico ? new UserDTO(farmaceutico) : null;
}

service.create = function (aviamento) {
  return new Promise(function (resolve, reject) {
    userService.findById(aviamento.farmaceutico)
      .then(function (farmaceutico) {
        resolve(new AviamentoDTO(aviamento, farmaceutico));
      })
      .catch(function (error) {
        reject(error);
      });
  });
};

module.exports = service;

