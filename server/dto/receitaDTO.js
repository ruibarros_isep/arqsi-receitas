var prescricaoDTO = require('../dto/prescricaoDTO');
var UserDTO = require('../dto/userDTO');
var userService = require('../services/userService');

var service = {};

function ReceitaDTO(receita, utente, medico, prescricoes) {
  this._id = receita._id;
  this.nome = receita.nome;
  this.utente = utente ? new UserDTO(utente): null;
  this.medico = medico ? new UserDTO(medico): null;
  this.data = receita.data;
  this.prescricoes = prescricoes;

}

service.create = function (receita) {
    return new Promise(function (resolve, reject) {
        var utentePromise = userService.findById(receita.utente);
        var medicoPromise = userService.findById(receita.medico);

        Promise.all([utentePromise, medicoPromise])
          .then(function (values) {
              var utente = values[0];
              var medico = values[1];

              var prescricoesPromises = receita.prescricoes.map(function (prescricao) {
                  return prescricaoDTO.create(prescricao);
              });

              Promise.all(prescricoesPromises).then(function (prescricoes) {
                  resolve(new ReceitaDTO(receita, utente, medico, prescricoes));
              })
        });

    });
};

service.createAll = function (receitas) {
    return receitas.map(function (receita) {
        return service.create(receita).then(function (receita) {

            return receita;
        });
    });
};

module.exports = service;
