var aviamentoDTO = require('../dto/aviamentoDTO');

var service = {};

function PrescricaoDTO(prescricao, aviamentos) {
  this._id = prescricao._id;
  this.apresentacao = prescricao.apresentacao;
  this.apresentacaoId = prescricao.apresentacaoId;
  this.posologia = prescricao.posologia;
  this.posologiaId = prescricao.posologiaId;
  this.farmaco = prescricao.farmaco;
  this.quantidade = prescricao.quantidade;
  this.aviada = prescricao.aviada;
  this.alerta = prescricao.alerta;
  this.validade = prescricao.validade;
  this.aviamentos = aviamentos;
}

service.create = function (prescricao) {
    return new Promise(function (resolve, reject) {

        var aviamentosPromises = prescricao.aviamentos.map(function (aviamento) {
            return aviamentoDTO.create(aviamento);
        });

        Promise.all(aviamentosPromises).then(function (aviamentos) {
            resolve(new PrescricaoDTO(prescricao, aviamentos));
        });

    });

};

module.exports = service;
