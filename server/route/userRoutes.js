var userService = require('../services/userService');
var authenticationService = require("../services/authenticationService");
var authorizationservice = require("../services/authorizationService");
var check = require("express-validator/check");
var validationResult = require("express-validator/check/validation-result");
var UserDTO = require('../dto/userDTO');
var express = require("express");
var router = express.Router();

router
    .route("/")
    .get(authenticationService, authorizationservice.userIsMedicoOrFarmaceutico, function (req, res) {
        userService
            .findAll()
            .then(function (users) {

                var usersDTO = users.map(function (user) {
                   return new UserDTO(user);
                });

                res.json(usersDTO);
            })
            .catch(function (error) {
                res.status(500).send(error);
            });
    })
    .post(
        check.check('nome').exists(),
        check.check('email').isEmail(),
        check.check('password').isLength({min: 8}),
        check.check('medico').isBoolean(),
        check.check('farmaceutico').isBoolean(),
        function (req, res) {

            var errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({errors: errors.mapped()});
            }

            userService
                .create(
                    req.body.nome,
                    req.body.password,
                    req.body.email,
                    req.body.medico,
                    req.body.farmaceutico
                )
                .then(function (user) {

                    res.json(new UserDTO(user));
                })
                .catch(function (error) {
                    res.status(500).send(error);
                });
        });

router.route('/:user_id')
  .get(authenticationService, function (req, res) {
      userService.findById(req.param('user_id'))
        .then(function (user) {
            res.json(new UserDTO(user));
        })
        .catch(function (error) {
            res.status(404).send(error);
        })
  });

module.exports = router;
