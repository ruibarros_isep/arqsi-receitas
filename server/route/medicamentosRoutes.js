var medicamentosService = require('../services/medicamentosService');
var authenticationService = require('../services/authenticationService');
var authorizationService = require('../services/authorizationService');
var express = require("express");
var router = express.Router();

router.route('/').get(function (req, res) {
  medicamentosService.getMedicamentos().then(function (medicamentos) {
    res.json(medicamentos);
  }).catch(function (error) {
    res.status(500).send(error);
  })
});

router.route('/:medicamento_id/apresentacoes').get(function (req, res) {
  var medicamentoId = req.param('medicamento_id')
  medicamentosService.getApresentacoesByMedicamentoId(medicamentoId).then(function (apresentacoes) {
    res.json(apresentacoes);
  }).catch(function (error) {
    res.status(500).send(error);
  })
});

router.route('/apresentacao/:apresentacao_id/comentario')
  .post(authenticationService, authorizationService.userIsMedico, function (req, res) {
    var apresentacao_id = req.param('apresentacao_id');
    var medicoId = req.user._id;
    var comment = req.body.comentario;

    medicamentosService.postComment(apresentacao_id, medicoId, comment).then(function (comentario) {
      res.json(comentario);
    }).catch(function (error) {
      res.status(500).send(error);
    })
  });

module.exports = router;
