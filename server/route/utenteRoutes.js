var receitaRepository = require("../repositories/receitaRepository");
var authenticationService = require("../services/authenticationService");
var authorizationService = require("../services/authorizationService");
var PrescricaoDTO = require('../dto/prescricaoDTO');
var express = require("express");
var router = express.Router();

router.route("/:user_id/prescricao/poraviar")
    .get(authenticationService, function (req, res) {

        receitaRepository
            .findPrescricoesPorAviarByUtente(req.params.user_id, req.query.data)
            .then(function (prescricoes) {
              var prescricoesDTO = prescricoes.map(function (prescricao) {
                return new PrescricaoDTO(prescricao);
              });

              res.send(prescricoesDTO);
            })
            .catch(function (error) {
                res.status(500).send(error);
            });
    });

module.exports = router;
