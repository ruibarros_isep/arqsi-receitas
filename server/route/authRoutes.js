var config = require("../../config");
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
var userRepository = require("../repositories/userRepository");
var express = require("express");
var check = require("express-validator/check");
var validationResult = require("express-validator/check/validation-result");
var UserDTO = require('../dto/userDTO');
var router = express.Router();

router.post('/login',
    check.check('email').isEmail(),
    check.check('password').exists(),
    function (req, res) {
        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({errors: errors.mapped()});
        }
        userRepository.findOneByEmail(req.body.email)
            .then(function (user) {
                if (user === null) {
                    res.status(404).send("Not found");
                }

                if (!bcrypt.compareSync(req.body.password, user.password)) {
                    res.status(401).send("Authentication failed");
                }

                var token = jwt.sign(
                    {
                        user: new UserDTO(user)
                    },
                    config.web.secret,
                    {
                        expiresIn: config.web.tokens_ttl
                    }
                );

                res.send({success: true, token: token})

            })
            .catch(function (error) {
                res.status(500).send(error);
            });
    });


module.exports = router;