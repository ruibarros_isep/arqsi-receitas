var receitaRepository = require("../repositories/receitaRepository");
var userRepository = require("../repositories/userRepository");
var medicamentosService = require("../services/medicamentosService");
var authenticationService = require("../services/authenticationService");
var authorizationService = require("../services/authorizationService");
var emailService = require('../services/mailgunEmailService');
var check = require("express-validator/check");
var validationResult = require("express-validator/check/validation-result");
var receitaDTO = require('../dto/receitaDTO');
var prescricaoDTO = require('../dto/prescricaoDTO');
var express = require("express");
var router = express.Router();

router
    .route("/")
    .get(authenticationService, function (req, res) {
        receitaRepository
            .findByUtenteId(req.user._id)
            .then(function (receitas) {

                var receitasDTOPromises = receitaDTO.createAll(receitas);

                Promise.all(receitasDTOPromises).then(function (receitasDTO) {
                    res.json(receitasDTO);
                });

            })
            .catch(function (error) {
                res.status(500).send(error);
            });
    })
    .post(authenticationService, authorizationService.userIsMedico,
        check.check('utente').isHexadecimal(),
        function (req, res) {

            var errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({errors: errors.mapped()});
            }

            var userPromise = userRepository.findById(req.body.utente);

            var receitaPromise = receitaRepository
                .create(
                    req.body.utente,
                    req.user._id,
                    []
                );

            Promise.all([userPromise, receitaPromise])
                .then(function (values) {

                    var user = values[0];
                    var receita = values[1];

                    emailService.sendReceitaWarning(user.email, user.nome, receita.id);

                    receitaDTO.create(receita).then(function (dto) {
                      res.json(dto);
                    });
                })
                .catch(function (error) {
                    res.status(500).send(error);
                });
        });

router
  .route("/medico")
  .get(authenticationService, authorizationService.userIsMedico, function (req, res) {
    receitaRepository
      .findByMedicoId(req.user._id)
      .then(function (receitas) {
        var receitasDTOPromises = receitaDTO.createAll(receitas);

        Promise.all(receitasDTOPromises).then(function (receitasDTO) {
            res.json(receitasDTO);
        });
      })
      .catch(function (error) {
        res.status(500).send(error);
      });
  });

router.route("/:receita_id").get(authenticationService, function (req, res) {
    receitaRepository
        .findById(req.params.receita_id)
        .then(function (receita) {
            if (receita === null) {
                res.status(404).send("Not found");
            }

            if (!authorizationService.userIsOwnerOfReceitaOrMedicoOfReceitaOrFarmaceutico(req.user, receita)) {
                return res.status(401).send("You are not allowed to access this resource");
            }

          receitaDTO.create(receita).then(function (receita) {
              res.json(receita);
          });

        })
        .catch(function (error) {
            res.status(500).send(error);
        });
});

router.route("/:receita_id/prescricao").post(authenticationService,
    check.check('apresentacao').isInt(),
    check.check('posologia').isInt(),
    check.check('quantidade').isInt(),
    function (req, res) {

        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({errors: errors.mapped()});
        }

        receitaRepository
            .findById(req.params.receita_id)
            .then(function (receita) {
                if (receita === null) {
                    return res.status(404).send("Not found");
                }

                if (!authorizationService.userMedicoIsOwnerOfReceita(req.user, receita)) {
                    return res.status(401).send("You are not allowed to access this resource. " +
                        "You should be the medico that created the prescricao");
                }

                medicamentosService.getPrescricao(req.body.apresentacao, req.body.posologia)
                    .then(function (prescricao) {

                        console.log(prescricao);

                        receitaRepository
                            .createPrescricao(receita,
                                prescricao.apresentacao,
                                req.body.apresentacao,
                                prescricao.posologia,
                                req.body.posologia,
                                prescricao.farmaco,
                                req.body.quantidade)
                            .then(function (receita) {
                              receitaDTO.create(receita).then(function (dto) {
                                res.json(dto);
                              });
                            })
                            .catch(function (error) {
                                res.status(500).send(error);
                            });

                    })
                    .catch(function (error) {
                        res.status(500).send(error);
                    });

            })
            .catch(function (error) {
                res.status(500).send(error);
            });
    });

router.route("/:receita_id/prescricao/:prescricao_id").get(authenticationService, function (req, res) {

    receitaRepository.findById(req.params.receita_id)
        .then(function (receita) {
            if (!authorizationService.userIsOwnerOfReceitaOrMedicoOfReceitaOrFarmaceutico(req.user, receita)) {
                return res.status(401).send("You are not allowed to access this resource.");
            }

            receitaRepository.findPrescricaoByIdAndReceitaId(receita._id, req.params.prescricao_id)
                .then(function (prescricao) {
                  prescricaoDTO.create(prescricao).then(function (dto) {
                    res.json(dto);
                  });
                })
                .catch(function (error) {
                    res.status(404).send(error);
                });

        }).catch(function (error) {
        res.status(500).send(error);
    });
}).put(authenticationService,
    check.check('apresentacao').isInt(),
    check.check('posologia').isInt(),
    check.check('quantidade').isInt(), function (req, res) {
        receitaRepository.findById(req.params.receita_id)
            .then(function (receita) {
                if (receita === null) {
                    res.status(404).send("Not found");
                }

                if (!authorizationService.userMedicoIsOwnerOfReceita(req.user, receita)) {
                    return res.status(401).send("You are not allowed to access this resource. " +
                        "You should be the medico that created the prescricao");
                }

                receitaRepository.findPrescricaoByIdAndReceitaId(receita._id, req.params.prescricao_id)
                    .then(function (prescricao) {

                        if (prescricao.aviamentos.length !== 0) {
                            return res.status(400).send("Prescricao ja tem aviamentos");
                        }

                        medicamentosService.getPrescricao(req.body.apresentacao, req.body.posologia)
                            .then(function (medicamentosPrescricao) {
                                receitaRepository.updatePrescricao(receita._id,
                                    prescricao._id,
                                    medicamentosPrescricao.apresentacao,
                                    req.body.apresentacao,
                                    medicamentosPrescricao.posologia,
                                    req.body.posologia,
                                    medicamentosPrescricao.farmaco,
                                    req.body.quantidade)
                                    .then(function (prescricao) {

                                        receitaRepository.findPrescricaoByIdAndReceitaId(receita._id, req.params.prescricao_id)
                                            .then(function (prescricao) {
                                              prescricaoDTO.create(prescricao).then(function (dto) {
                                                res.json(dto);
                                              });
                                            })
                                            .catch(function (error) {
                                                res.status(500).send(error);
                                            });

                                    })
                                    .catch(function (error) {
                                        res.status(500).send(error);
                                    });
                            })
                            .catch(function (error) {
                                res.status(404).send(error);
                            });


                    })
                    .catch(function (error) {
                        res.status(500).send(error);
                    });

            })
            .catch(function (error) {
                res.status(500).send(error);
            })
    });

router.route("/:receita_id/prescricao/:prescricao_id/alerta")
  .put(authenticationService,
    check.check('alerta').exists(),
    function (req, res) {

      receitaRepository.findById(req.params.receita_id).then(function (receita) {

        if (!authorizationService.userIsOwnerOfReceita(req.user, receita)) {
          return res.status(401).send('User has to be the owner of receita');
        }

        receitaRepository.findPrescricaoByIdAndReceitaId(req.params.receita_id, req.params.prescricao_id).then(function (prescricao) {

          receitaRepository.updateAlertaPrescricao(receita._id, prescricao._id, req.body.alerta).then(function (receita) {
            prescricao.alerta = req.body.alerta;
            prescricaoDTO.create(prescricao).then(function (dto) {
              res.json(dto);
            });

          }).catch(function (error) {
            res.status(500).send(error);
          });

        }).catch(function (error) {
          res.status(500).send(error);
        });
      }).catch(function (error) {
        res.status(500).send(error);
      });

    });

router
    .route("/:receita_id/prescricao/:prescricao_id/aviar")
    .put(authenticationService,
        authorizationService.userIsFarmaceutico,
        check.check('quantidade').isInt(),
        function (req, res) {

            var errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({errors: errors.mapped()});
            }

            receitaRepository.findPrescricaoByIdAndReceitaId(req.params.receita_id, req.params.prescricao_id)
                .then(function (prescricao) {

                    var total = receitaRepository.countQuantidadeAviamentos(prescricao);

                    if ((total + parseInt(req.body.quantidade)) > parseInt(prescricao.quantidade)) {
                        return res.status(400).send("Limite maximo atingido para prescricao: " + prescricao.quantidade);
                    }

                    receitaRepository
                        .criarAviamento(req.params.receita_id, req.params.prescricao_id, req.body.quantidade, req.user._id)
                        .then(function (success) {

                            if (!success) {
                                res.status(500).send({message: "Not able to add aviamento"});
                            }

                            if ((total + parseInt(req.body.quantidade)) === parseInt(prescricao.quantidade)) {
                                return receitaRepository.markPrescricaoAsAviada(req.params.receita_id, req.params.prescricao_id)
                                    .then(function () {

                                        receitaRepository.findPrescricaoByIdAndReceitaId(req.params.receita_id, req.params.prescricao_id)
                                            .then(function (prescricao) {
                                              prescricaoDTO.create(prescricao).then(function (dto) {
                                                res.json(dto);
                                              })
                                            })
                                            .catch(function (error) {
                                                res.status(500).send(error);
                                            });

                                    })
                                    .catch(function (error) {
                                        res.status(500).send(error);
                                    });
                            }

                            receitaRepository.findPrescricaoByIdAndReceitaId(req.params.receita_id, req.params.prescricao_id)
                                .then(function (prescricao) {
                                  prescricaoDTO.create(prescricao).then(function (dto) {
                                    res.json(dto);
                                  });
                                })
                                .catch(function (error) {
                                    res.status(500).send(error);
                                });

                        })
                        .catch(function (error) {
                            res.status(500).send(error);
                        });

                })
                .catch(function (error) {
                    res.status(500).send(error);
                });
        });

module.exports = router;
