
var mailgun = require('mailgun.js');
var config = require('../../config');

var mg = mailgun.client({
    username: config.smtp.mailgun.username,
    key: config.smtp.mailgun.apiKey
});


var emailService = {};

emailService.sendEmail = function (recipients, subject, textBody, htmlBody) {

    var data = {
        from: config.smtp.mailgun.sender,
        to: recipients,
        subject: recipients,
        text: textBody,
        html: htmlBody
    };

    return mg.messages.create(config.smtp.mailgun.domain, data)
        .then(function (message) {
            console.log(message);
        }).catch(function (error) {
            console.log(error);
        });

};

emailService.sendWelcomeEmail = function (email, nome) {

    var subject = "Registo em Gestão de Receitas ";
    var textBody = "Bem vindo à Gestão de Receitas " + nome;
    var htmlBody = "<h1>Bem vindo a Gestão de Receitas " + nome + "</h1>";

    return emailService.sendEmail([email], subject, textBody, htmlBody);

};

emailService.sendReceitaWarning = function (email, nome, receita_id) {

    var subject = "Nova receita - " + receita_id;
    var textBody = "Olá " + nome + " existe uma nova receita no sistema com o codigo " + receita_id;
    var htmlBody = "<p>Olá " + nome + " existe uma nova receita para si. Pode consulta-la em " + config.web.domain + "/receita/" + receita_id + " </p>";

    return emailService.sendEmail([email], subject, textBody, htmlBody);

};

emailService.sendAlertEmail = function (user, receita, prescricao) {

    var subject = "Aviso prescrição a expirar";
    var textBody = "Olá " + user.nome + " existe uma prescrição a expirar a " + prescricao.validade + ". Pode consulta-la em " + config.web.domain + "/receita/" + receita._id + "/prescricao/" + prescricao._id;
    var htmlBody = "<p>Olá " + user.nome + " existe uma prescrição a expirar a " + prescricao.validade  + ". Pode consulta-la em " + config.web.domain + "/receita/" + receita._id + "/prescricao/" + prescricao._id + " </p>";

    return emailService.sendEmail([user.email], subject, textBody, htmlBody);


};


module.exports = emailService;
