
var jwt = require("jsonwebtoken");
var config = require("../../config");

function verifyToken(req, res, next) {
    var token = req.headers['x-access-token'];
    if (!token){
        return res.status(403).send("Token required");
    }

    jwt.verify(token, config.web.secret, function (error, decoded) {
        if (error){
            return res.status(500).send("Authentication Failed");
        }

        req.user = decoded.user;
        next();

    });
}

module.exports = verifyToken;