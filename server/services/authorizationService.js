
var authorizationService = {};

authorizationService.userIsMedicoOrFarmaceutico = function (req, res, next) {

    if(!req.user){
        return res.status(401).send("Unauthorized");
    }

    if (!(req.user.medico || req.user.farmaceutico)){
        return res.status(401).send("User should be medico or farmaceutico");
    }

    next();
};

authorizationService.userIsOwnerOrMedicoOrFarmaceutico = function (req, res, next) {

    if (req.user.medico || req.user.farmaceutico){
        next();
    }

    if (req.user._id !== req.params.user_id){
        return res.status(401).send("You are not allowed to access this resource");
    }

    next();

};

authorizationService.userIsOwnerOfReceitaOrMedicoOfReceitaOrFarmaceutico = function (user, receita) {
    if (user._id == receita.utente){
        return true;
    }

    if (user._id == receita.medico){
        return true;
    }

    return user.farmaceutico;
};

authorizationService.userIsMedico = function(req, res, next){
    if (!req.user.medico){
        return res.status(401).send("You are not allowed to perform this action. You should be medico");
    }

    next();
};

authorizationService.userMedicoIsOwnerOfReceita = function (user, receita) {

    return receita.medico == user._id;
};

authorizationService.userIsFarmaceutico = function (req, res, next) {
    if (!req.user.farmaceutico){
        return res.status(401).send("You are not allowed to perform this action. You should be farmaceutico");
    }
    next();
};

authorizationService.userIsOwnerOfReceita = function (user, receita) {

  return receita.utente == user._id;
};

module.exports = authorizationService;
