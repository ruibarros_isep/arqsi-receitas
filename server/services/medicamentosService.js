var medicamentosApiClient = require("../clients/medicamentosApiClient");
var cacheService = require('./cacheService');
var config = require('../../config');

var medicamentosService = {};

const TOKEN_KEY = "MEDICAMENTOS_TOKEN";
const MEDICAMENTOS_TTL = 300;

function authenticate() {
    return new Promise(function (resolve, reject) {

        var token = cacheService.get(TOKEN_KEY);

        if (token){
            console.log("Fetched token from cache " + token);
            resolve(token);
            return;
        }

        var authArgs = {
            data: {
                email: config.medicamentosApi.email,
                password: config.medicamentosApi.password
            },
            headers: {
                "Content-Type": "application/json"
            }
        };

        medicamentosApiClient.methods.requestToken(authArgs, function (auth, response) {
            if(!auth.token){
                console.log("ERROR: getting token from Medicamentos Auth");
                reject("Error on medicamentos authentication");
                return;
            }

            console.log("Fetched token from Medicamentos Auth: " + auth.token);
            cacheService.set(TOKEN_KEY, auth.token, MEDICAMENTOS_TTL);
            resolve(auth.token);
        });
    })
}

function getApresentcaoById(id, token) {
    return new Promise(function (resolve, reject) {

        var args = {
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            path: {
                id: id
            }
        };

        medicamentosApiClient.methods.getApresentcaoById(args, function (apresentacao, response) {

            if (!('forma' in apresentacao)||
                !('concentracao' in apresentacao) ||
                !('qtd' in apresentacao) ||
                !('farmacoId' in apresentacao) ||
                !('medicamentoId' in apresentacao)) {
                reject("Error in medicamentos API: /api/Apresentcao/" + id);
            }

            resolve(apresentacao);
        });
    });

};

function getPosologiaById(id, token) {
    return new Promise(function (resolve, reject) {
        var args = {
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            path: {
                id: id
            }
        };

        medicamentosApiClient.methods.getPosologiaById(args, function (posologia, response) {

            if (!('quantidade' in posologia) || !('frequencia' in posologia) || !('via' in posologia) || !('duracao' in posologia)) {
                reject("Error in medicamentos API: /api/Posologias/"+id);
            }

            resolve(posologia);
        });
    });
};

function getFarmacoById(id, token) {
    return new Promise(function (resolve, reject) {

        var args = {
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            path: {
                id: id
            }
        };

        medicamentosApiClient.methods.getFarmacoById(args, function (farmaco, response) {
            if (!('farmacoNome' in farmaco)) {
                reject("Error in medicamentos API: /api/Farmacos/" + id);
            }

            resolve(farmaco);

        });
    });
};

function getMedicamentoById(id, token) {
    return new Promise(function (resolve, reject) {

        var args = {
            headers: {
               "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            path: {
                id: id
            }
        };

        medicamentosApiClient.methods.getMedicamentoById(args, function (medicamento, response) {
            if (!('nome' in medicamento)) {
                reject("Error in medicamentos API: /api/Medicamentos/" + id);
            }

            resolve(medicamento);

        });
    });
};

function getApresentacaoAsString (apresentacao, medicamento){
    return "Medicamento: " + medicamento.nome +
        " ,Forma: " + apresentacao.forma +
        ", Concentração: " + apresentacao.concentracao +
        ", Quantidade: " + apresentacao.qtd;
};

function getPosologiaAsString(posologia){
    return "Via: " + posologia.via +
        ", Quantidade: " + posologia.quantidade +
        ", Frequencia: " + posologia.frequencia +
        ", Duracao: " + posologia.duracao;
};

medicamentosService.getPrescricao = function (apresentacaoId, posologiaId) {

    return new Promise(function (resolve, reject) {
        //TODO check cache
        authenticate()
            .then(function (token) {

                var prescricao = {};

                var apresentacaoPromise = getApresentcaoById(apresentacaoId, token);
                var posologiaPromise = getPosologiaById(posologiaId, token);

                Promise.all([apresentacaoPromise, posologiaPromise])
                    .then(function (values) {
                        var apresentacao = values[0];
                        var posologia = values[1];

                        var farmacoPromise = getFarmacoById(apresentacao.farmacoId, token);
                        var medicamentoPromise = getMedicamentoById(apresentacao.medicamentoId, token);

                        Promise.all([farmacoPromise, medicamentoPromise])
                            .then(function (values) {
                                var farmaco = values[0];
                                var medicamento = values[1];

                                //TODO should we validate if posologia is valid for medicamento

                                prescricao.apresentacao = getApresentacaoAsString(apresentacao, medicamento);
                                prescricao.posologia = getPosologiaAsString(posologia);
                                prescricao.farmaco = farmaco.farmacoNome;

                                resolve(prescricao);

                            })
                            .catch(function (error) {
                                reject(error);
                            });

                    })
                    .catch(function (error) {
                        reject(error);
                    });

            })
            .catch(function (error) {
                reject(error);
            });

    });
};

medicamentosService.getMedicamentos = function () {

  return new Promise(function (resolve, reject) {
      authenticate().then(function (token) {

        var args = {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          }
        };

        medicamentosApiClient.methods.getMedicamentos(args, function (medicamentos, response) {
          resolve(medicamentos);
        });

      }).catch(function (error) {
        reject(error);
      });
  });

};

medicamentosService.getApresentacoesByMedicamentoId = function (medicamentoId) {

  return new Promise(function (resolve, reject) {
    authenticate().then(function (token) {

      var args = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        path: {
          id: medicamentoId
        }
      };

      medicamentosApiClient.methods.getApresentcoesByMedicamentoId(args, function (apresentacoes, response) {
        resolve(apresentacoes);
      });

    }).catch(function (error) {
      reject(error);
    });
  });

};

medicamentosService.postComment = function (apresentacaoId, medicoId, comment) {

  return new Promise(function (resolve, reject) {
    authenticate().then(function (token) {

      var args = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        data: {
          medicoId: medicoId,
          comentario: comment,
          apresentacaoId: apresentacaoId
        },
      };

      medicamentosApiClient.methods.postComment(args, function (comentario, response) {
        resolve(comentario);
      });

    }).catch(function (error) {
      reject(error);
    });
  });

};



module.exports = medicamentosService;
