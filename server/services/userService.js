var userRepository = require("../repositories/userRepository");
var emailService = require('../services/mailgunEmailService');

userService = {};

userService.findById = function (id) {
  return userRepository.findById(id);
};

userService.findAll = function () {
  return userRepository.findAll();
};

userService.create = function (nome,
                               password,
                               email,
                               medico,
                               farmaceutico) {
  return new Promise(function (resolve, reject) {
    userRepository.create(nome,
      password,
      email,
      medico,
      farmaceutico)
      .then(function (user) {
        emailService.sendWelcomeEmail(user.email, user.nome);
        resolve(user);
      }).catch(function (error) {
      reject(error);
    });

  });

};

module.exports = userService;
