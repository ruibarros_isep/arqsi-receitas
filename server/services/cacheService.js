
const NodeCache = require( "node-cache" );
const cache = new NodeCache({ stdTTL: 100, checkperiod: 120 });

var cacheService = {};

cacheService.set = function (key, value, ttl) {
    return new Promise(function (resolve, reject) {
        cache.set(key, value, ttl, function (error, success) {
            if (error){
                console.log("ERROR: Could not set cache key: " +  key + " with value: " + value );
                reject(error);
            }
            console.log("Successfully set cache key: " +  key + " with value: " + value );
            resolve(success);
        });
    });
};


cacheService.get = function (key) {
  return cache.get(key);
};

module.exports = cacheService;