var config = require('../../config');
var receitaRepository = require('../repositories/receitaRepository');
var userRepository = require('../repositories/userRepository');
var emailService = require('../services/mailgunEmailService');
var mongoose = require("mongoose");

mongoose.connect(
  "mongodb://" +
  config.mongodb.host +
  ":" +
  config.mongodb.port +
  "/" +
  config.mongodb.database
);

var sendAlertTask = {};

function setAlertaEnviado(prescricao, receita) {

  return new Promise(function (resolve, reject) {
    prescricao.alertaEnviado = true;
    receita.save(function () {

      console.log("Alerta enviado registado for prescricao " + prescricao._id);
      resolve(prescricao);

    }, function (error) {
      console.log(error);
      reject(error);

    });

  });
}

function processPrescricao(receita, prescricao, user) {

  console.log("Found prescricao por aviar " + prescricao._id);

  return new Promise(function (resolve, reject) {
    var now = new Date();

    if (prescricao.alertaEnviado) {
      console.log("Prescricao " + prescricao._id + " já enviado");
      resolve(prescricao);
      return;
    }

    if (now < prescricao.alerta) {
      console.log("Prescricao " + prescricao._id + " not time yet");
      resolve(prescricao);
      return;
    }

    emailService.sendAlertEmail(user, receita, prescricao);

    setAlertaEnviado(prescricao, receita).then(function (prescricao) {
        resolve(prescricao);
    }).catch(function (error) {
        reject(error);
    });

  });
}

function processReceita(receita, user) {
  console.log("Found prescricoes por aviar on receita " + receita._id);

  return new Promise(function (resolve, reject) {
    var promises = receita.prescricoes.map(function (prescricao) {
      return processPrescricao(receita, prescricao, user);
    });

    Promise.all(promises).then(function (values) {
        resolve(receita);
    }).catch(function (error) {
        reject(error);
    });

  });
}

function processUser(user) {
  console.log("Check if " + user._id + " has prescricoes por aviar");

  return new Promise(function (resolve, reject) {
    var now = new Date();
    receitaRepository.findPrescricoesPorAviarByUtente(user._id, now)
      .then(function (receitas) {

        var promises = receitas.map(function (receita) {

          return processReceita(receita, user);
        });

        Promise.all(promises).then(function (values) {
           resolve(user);
        }).catch(function (error) {
            reject(error)
        });

      });

  });
}

var start = function() {
  console.log("Start processing...");
  userRepository.findAll().then(function (users) {

    console.log("Found " + users.length + " users");

    var promises = users.map(function (user) {
      return processUser(user);

    });

    Promise.all(promises).then(function (values) {
        process.exit();
    }).catch(function (error) {
      console.log(error);
      process.exit(1);
    });

  });

};

sendAlertTask.start = start;

module.exports = sendAlertTask;

