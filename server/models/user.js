var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    nome: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    medico: {
        type: Boolean,
        required: true,
        default: false
    },
    farmaceutico: {
        type: Boolean,
        required: true,
        default: false
    }
});

module.exports = mongoose.model("User", UserSchema);
