var mongoose = require("mongoose");
var idvalidator = require('mongoose-id-validator');

var Schema = mongoose.Schema;

//TODO add id autoincrement
var PrescricaoSchema = new Schema({
    apresentacao: String,
    apresentacaoId: String,
    posologia: String,
    posologiaId: String,
    farmaco: String,
    quantidade: Number,
    aviada: {
        type: Date,
        default: null
    },
    alerta: {
      type: Date,
      default: + new Date() + 24*24*60*60*1000 //+3weeks
    },
    alertaEnviado: {
      type: Boolean,
      default: false
    },
    validade: {
        type: Date,
        default: + new Date() + 30*24*60*60*1000 //+1month
    },
    aviamentos: [{
        farmaceutico: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        data: {
            type: Date,
            default: Date.now()
        },
        quantidade: Number
    }]
});

var ReceitaSchema = new Schema({
    utente: {
        type: Schema.ObjectId,
        ref: "User"
    },
    medico: {
        type: Schema.ObjectId,
        ref: "User"
    },
    data: {
        type: Date,
        default: Date.now()
    },
    prescricoes: [PrescricaoSchema]
});

ReceitaSchema.plugin(idvalidator);
PrescricaoSchema.plugin(idvalidator);

module.exports = mongoose.model("Receita", ReceitaSchema);
