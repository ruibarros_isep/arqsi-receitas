var config = require('../../config');
var Client = require('node-rest-client').Client;
var medicamentosApiClient = new Client();

medicamentosApiClient.registerMethod('requestToken', config.medicamentosApi.url + config.medicamentosApi.token, 'POST');
medicamentosApiClient.registerMethod('getMedicamentos', config.medicamentosApi.url + config.medicamentosApi.medicamento, 'GET');
medicamentosApiClient.registerMethod('getMedicamentoById', config.medicamentosApi.url + config.medicamentosApi.medicamento + '/${id}', 'GET');
medicamentosApiClient.registerMethod('getApresentcaoById', config.medicamentosApi.url + config.medicamentosApi.apresentacao + '/${id}', 'GET');
medicamentosApiClient.registerMethod('getApresentcoesByMedicamentoId', config.medicamentosApi.url + config.medicamentosApi.medicamento + '/${id}' + config.medicamentosApi.apresentacao, 'GET');
medicamentosApiClient.registerMethod('getPosologiaById', config.medicamentosApi.url + config.medicamentosApi.posologia + '/${id}', 'GET');
medicamentosApiClient.registerMethod('getFarmacoById', config.medicamentosApi.url + config.medicamentosApi.farmaco + '/${id}', 'GET');
medicamentosApiClient.registerMethod('postComment', config.medicamentosApi.url + config.medicamentosApi.comentario, 'POST');

module.exports = medicamentosApiClient;
