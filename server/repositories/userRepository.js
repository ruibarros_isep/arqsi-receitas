var User = require("../models/user");
var bcrypt = require("bcryptjs");

var userRepository = {};

userRepository.findOneByEmail = function (email) {
    return new Promise(function (resolve, reject) {
        User.findOne({email: email}, function (error, user) {
            if (error){
                reject(error);
            }
            resolve(user);
        });
    });
};

userRepository.findAll = function () {
    return new Promise(function (resolve, reject) {
        User.find(function (error, users) {
            if (error) {
                reject(error);
            }
            resolve(users);
        });
    });
};

userRepository.create = function (nome, password, email, medico, farmaceutico) {
    return new Promise(function (resolve, reject) {
        var user = new User();
        user.nome = nome;
        user.password = bcrypt.hashSync(password, 8);
        user.email = email;
        user.medico = medico;
        user.farmaceutico = farmaceutico;
        user.save(function (error) {
            if (error) {
                reject(error);
            }
            resolve(user);
        });
    });
};

userRepository.findById = function (user_id) {
    return new Promise(function (resolve, reject) {
         User.findById(user_id, function (error, user) {
             if (error){
                 reject(error);
             }
             resolve(user);
         });
    });
};

module.exports = userRepository;
