var Receita = require("../models/receita");

var receitaRepository = {};

receitaRepository.findByUtenteId = function (utente_id) {
  return new Promise(function (resolve, reject) {
    Receita.find({utente: utente_id}, function (error, receitas) {
      if (error) {
        reject(error);
      }
      resolve(receitas);
    });
  });
};



receitaRepository.findByMedicoId = function (medico_id) {
    return new Promise(function (resolve, reject) {
        Receita.find({medico: medico_id}, function (error, receitas) {
            if (error) {
                reject(error);
            }
            resolve(receitas);
        });
    });
};

receitaRepository.create = function (utente, medico, prescricoes) {
    return new Promise(function (resolve, reject) {
        var receita = new Receita();
        receita.utente = utente;
        receita.medico = medico;
        receita.prescricoes = prescricoes;
        receita.save(function (error) {
            if (error) {
                reject(error);
            }
            resolve(receita);
        });
    });
};

receitaRepository.findById = function (id) {
    return new Promise(function (resolve, reject) {
        Receita.findById(id, function (error, receita) {
            if (error) {
                reject(error);
            }
            resolve(receita);
        });
    });
};

receitaRepository.createPrescricao = function (receita,
                                               apresentacao,
                                               apresentacaoId,
                                               posologia,
                                               posologiaId,
                                               farmaco,
                                               quantidade) {
    return new Promise(function (resolve, reject) {
        receita.prescricoes.push({
            apresentacao: apresentacao,
            apresentacaoId: apresentacaoId,
            posologia: posologia,
            posologiaId: posologiaId,
            farmaco: farmaco,
            quantidade: quantidade,
            aviamentos: []
        });
        receita.save(function (error) {
            if (error) {
                reject(error);
            }
            resolve(receita);
        });
    });
};


receitaRepository.updatePrescricao = function (receita_id, prescricao_id, apresentacao, apresentacaoId, posologia, posologiaId, farmaco, quantidade) {
    return new Promise(function (resolve, reject) {
        Receita.update({
                '_id': receita_id,
                'prescricoes._id': prescricao_id
            },
            {
                $set: {
                    'prescricoes.$.apresentacao': apresentacao,
                    'prescricoes.$.apresentacaoId': apresentacaoId,
                    'prescricoes.$.posologia': posologia,
                    'prescricoes.$.posologiaId': posologiaId,
                    'prescricoes.$.farmaco': farmaco,
                    'prescricoes.$.quantidade': quantidade
                }
            }, function (error, prescricao) {
                if (error){
                    reject(error);
                }
                resolve(prescricao);
            });
    });
};

receitaRepository.updateAlertaPrescricao = function (receita_id, prescricao_id, alerta) {
  return new Promise(function (resolve, reject) {
    Receita.update({
        '_id': receita_id,
        'prescricoes._id': prescricao_id
      },
      {
        $set: {
          'prescricoes.$.alerta': alerta,
          'prescricoes.$.alertaEnviado': false,
        }
      }, function (error, prescricao) {
        if (error){
          reject(error);
        }
        resolve(prescricao);
      });
  });
};

receitaRepository.findPrescricaoByIdAndReceitaId = function (receita_id, prescricao_id) {
    return new Promise(function (resolve, reject) {

        Receita.findOne({
                "_id": receita_id,
                'prescricoes': {
                    $elemMatch: {
                        _id: prescricao_id
                    }
                }
            }, {
                'prescricoes.$': 1
            }, function (error, receita) {
                if (error) {
                    reject(error);
                }

                if (receita === null) {
                    return reject("Not found");
                }

                if (receita.prescricoes.$size === 0) {
                    reject("Not found");
                }

                resolve(receita.prescricoes.id(prescricao_id));
            }
        );
    });
};

receitaRepository.criarAviamento = function (receita_id, prescricao_id, quantidade, farmaceutico) {
    return new Promise(function (resolve, reject) {

        Receita.update({
            _id: receita_id,
            prescricoes: {
                $elemMatch: {
                    _id: prescricao_id
                }
            }
        }, {
            $push: {
                "prescricoes.$.aviamentos": {
                    farmaceutico: farmaceutico,
                    quantidade: quantidade
                }
            }
        }, function (error, model) {
            if (error) {
                reject(error);
            }
            resolve(model.ok);
        });

    });
};

receitaRepository.markPrescricaoAsAviada = function (receita_id, prescricao_id) {
    return new Promise(function (resolve, reject) {

        Receita.update(
            {
                _id: receita_id, 'prescricoes._id': prescricao_id
            }, {
                '$set': {
                    'prescricoes.$.aviada': new Date()
                }
            }, function (error, affected) {

                if (error) {
                    reject(error);
                }
                resolve(affected.ok);
            });

    });
};

//TODO improve this
receitaRepository.countQuantidadeAviamentos = function (prescricao) {
    return prescricao.aviamentos.reduce(function (accumulator, aviamento) {
        return accumulator + aviamento.quantidade;
    }, 0);
};

receitaRepository.findPrescricoesPorAviarByUtente = function (utente_id, data) {

    data = !data ? new Date() : data;

    return new Promise(function (resolve, reject) {

        Receita.find({
            "utente": utente_id,
            "data": {
                $lt: data
            }
        }, function (error, receitas) {
            if (error) {
                reject(error);
            }

            var receitasFiltered = receitas.map(function (receita) {
                receita.prescricoes = receita.prescricoes.filter(function (prescricao) {
                    return (prescricao.validade > data && (prescricao.aviada >= data || prescricao.aviada === null) );
                });

                return receita;

            });

            resolve(receitasFiltered);
        });
    });
};

module.exports = receitaRepository;
